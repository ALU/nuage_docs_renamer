import argparse
import os
import re
import string


def get_args():
    """
   Supports the command-line arguments listed below.
   """
    parser = argparse.ArgumentParser(
            description='Process args for retrieving all the Virtual Machines')
    parser.add_argument('-p', '--path', required=True, default='.', action='store',
                        help='Path to the directory with documentation sections')
    args = parser.parse_args()
    return args


def filename_formatter(s):
    valid_chars = "-_.() %s%s" % (string.ascii_letters, string.digits)
    filename = ''.join(c for c in s if c in valid_chars)
    filename = filename.replace(' ', '_')
    return filename


re_html_title = re.compile(r'<title>(.+)&mdash')


def main():
    args = get_args()
    if os.path.isdir(args.path):
        os.chdir(args.path)
        for doc_section_dir in os.listdir(os.curdir):
            if os.path.isdir(doc_section_dir):
                content = open(os.path.join(os.curdir, doc_section_dir, "index.html"), encoding="utf8").read()
                try:
                    new_dirname = filename_formatter(re_html_title.search(content).group(1).strip())
                except AttributeError:
                    break
                os.rename(os.path.join(os.curdir, doc_section_dir), os.path.join(os.curdir, new_dirname))


# Start program
if __name__ == "__main__":
    main()
