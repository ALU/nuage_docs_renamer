# Nuage Documentation Directories Renamer
This tiny python3 script renames documentation directories downloaded from OLCS (NOLS).
## What is the problem?
Nuage documentation comes in `html` format and once you download the whole collection's archive for a particular release you see something like that:
```
rdodin at rd in /tmp/test
$ tree -l1
.
├── 3HE10560AAAC
├── 3HE10723AAAC
├── 3HE10724AAAC
└── 3HE10726AAAC
```
As long as you did not memorize mappings between Document IDs and the human-readable titles of the docs, such representation is a bit of annoying.

## What will the script do?
This script will grab the title name embedded in the `index.html` of each directory and rename the directory accordingly.

## How to run it? What are the dependencies?
The only dependency is `python3` interpreter itself.
To run a script simply issue `python3 run.py -p /tmp/test/` specifying the root folder where unarchived directories with docs are.
You will end up with humanized directory names:
```
rdodin at rd in /tmp/test
$ tree -L 1
.
├── Nuage_VSP_CloudStack_4.3_Plugin_Guide
├── Nuage_VSP_Install_Guide
├── Nuage_VSP_OpenStack_Mitaka_Neutron_Plugin_User_Guide
└── Nuage_VSP_User_Guide
```
